<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('password');
            $table->text('salt');
            $table->string('email');
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('email_verified');
            $table->integer('active');
            $table->string('ip_address');
            $table->dateTime('created');
            $table->dateTime('modified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('password');
            $table->dropColumn('salt');
            $table->dropColumn('email');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('email_verified');
            $table->dropColumn('active');
            $table->dropColumn('ip_address');
            $table->dropColumn('created');
            $table->dropColumn('modified');
        });
    }
}
